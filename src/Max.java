

/**
 * Max calcule le max des valeurs vues, quelque soit le lot.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class Max extends Traitement {

	private double max = Double.NEGATIVE_INFINITY;

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		if (max > this.max)
			this.max = max;

	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": max = " + this.max);
	}

	public Double trouverMax() {
		for (Double valeur : donneeTraitee.values()) {
			setMax(valeur);
		}
		return getMax();
	}

}
