
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.collect.Multimap;

/**
 * Donnees enregistre toutes les données reçues, quelque soit le lot.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Donnees extends Traitement {
	private List<Integer> x = new ArrayList<>();
	private List<Integer> y = new ArrayList<>();
	private List<Double> valeur = new ArrayList<>();

	public Multimap<Position, Double> getDonneesType1(String nomFichier) {
		importerDonneesType1(nomFichier);
		return donneeTraitee;
	}

	public Multimap<Position, Double> getDonneesType2(String nomFichier) {
		importerDonneesType2(nomFichier);
		return donneeTraitee;
	}

	public void addDonnees(Position position, double valeur) {
		donneeTraitee.put(position, valeur);
	}

	public void importerDonneesType1(String nomFichier) {
		try (BufferedReader in = new BufferedReader(new FileReader(nomFichier))) {
			String ligne = null;
			while ((ligne = in.readLine()) != null) {
				String[] mots = ligne.split("\\s+");
				assert mots.length == 4; // 4 mots sur chaque ligne
				double valeur = Double.parseDouble(mots[3]);
				int x = Integer.parseInt(mots[0]);
				int y = Integer.parseInt(mots[1]);
				Position p = new Position(x, y);
				addDonnees(p, valeur);
			}
		} catch (IOException e) {
			System.out.println("File you have entered is not found ");
			System.out.println("Terminating process");
			System.exit(0);
			// throw new RuntimeException(e);
		}
	}

	public void importerDonneesType2(String nomFichier) {
		try (BufferedReader in = new BufferedReader(new FileReader(nomFichier))) {
			String ligne = null;
			while ((ligne = in.readLine()) != null) {
				String[] mots = ligne.split("\\s+");
				assert mots.length == 6; // 6 mots sur chaque ligne
				double valeur = Double.parseDouble(mots[4]);
				int x = Integer.parseInt(mots[1]);
				int y = Integer.parseInt(mots[2]);
				Position p = new Position(x, y);
				addDonnees(p, valeur);
			}
		} catch (IOException e) {
			System.out.println("File you have entered is not found ");
			System.out.println("Terminating process");
			System.exit(0);
			// throw new RuntimeException(e);
		}
	}

	public void importerDonneeXml(String nomFichier) {
		try {
			File file = new File("brut--genere.xml");
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = documentBuilder.parse(file);
			System.out.println("Root element: " + document.getDocumentElement().getNodeName());
			if (document.hasChildNodes()) {
				printNodeList(document.getChildNodes());
				if (x.size() == y.size() && x.size() == valeur.size() && y.size() == valeur.size()) {
					for (int i = 0; i < x.size(); i++) {
						addDonnees(new Position(x.get(i), y.get(i)), valeur.get(i));
					}
				} else {
					System.out.println("Some values are missing");
					throw new Exception();
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void printNodeList(NodeList nodeList) {
		for (int count = 0; count < nodeList.getLength(); count++) {
			Node elemNode = nodeList.item(count);
			if (elemNode.getNodeType() == Node.ELEMENT_NODE) {
				// get node name and value
				if (elemNode.getNodeName() == "valeur") {
					this.valeur.add(Double.parseDouble(elemNode.getTextContent()));
					System.out.println("Node Content =" + elemNode.getTextContent());
				}
				if (elemNode.getNodeName() == "abscisse" || elemNode.getNodeName() == "x") {
					this.x.add(Integer.parseInt(elemNode.getTextContent()));
					System.out.println("Node Content =" + elemNode.getTextContent());
				}
				if (elemNode.getNodeName() == "ordonnee" || elemNode.getNodeName() == "y") {
					System.out.println("Node Content =" + elemNode.getTextContent());
					this.y.add(Integer.parseInt(elemNode.getTextContent()));
				}

				if (elemNode.hasAttributes()) {
					NamedNodeMap nodeMap = elemNode.getAttributes();
					for (int i = 0; i < nodeMap.getLength(); i++) {
						Node node = nodeMap.item(i);
						if (node.getNodeName() == "y") {
							// System.out.println("attr name : " + node.getNodeName());
							this.y.add(Integer.parseInt(node.getTextContent()));
							System.out.println("attr value : " + node.getNodeValue());
						}
					}
				}
				if (elemNode.hasChildNodes()) {
					// recursive call if the node has child nodes
					printNodeList(elemNode.getChildNodes());
				}
				// System.out.println("Node Name =" + elemNode.getNodeName() + " [CLOSE]");
			}
		}
	}
	/*
	 * public static void main(String[] args) { new
	 * Donnees().importerDonneeXml("brut--genere.xml"); }
	 */
}
