
import java.util.Collection;

/**
 * Somme calcule la sommee des valeurs, quelque soit le lot.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class Somme extends SommeAbstrait {
	private double somme = 0;
	private boolean done = true;

	public void addToSomme(double valeur) {
		somme += valeur;
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": somme = " + this.somme());
		// this.somme = 0;// initialiser la somme � zero pour chaque fin lot
	}

	@Override
	public double somme() {
		return somme;
	}

	public void calculateSomme() {
		if (done) {
			for (Double valeur : donneeTraitee.values()) {
				this.somme += valeur;
			}
			done = false;
		}

	}

}
