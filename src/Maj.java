

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;

/**
 * Maj indique pour chaque lot les positions mises à jour (ou ajoutées) lors
 * du traitement de ce lot.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Donnees {

	ArrayList<Position> listeDesPositions = new ArrayList<Position>();

	public Maj() {

	}

	public void miseAJour(Position p) {
		if (listeDesPositions.isEmpty()) {
			listeDesPositions.add(p);
		} else {
			if (!listeDesPositions.contains(p))
				listeDesPositions.add(p);
		}

	}

	/**
	 * @return the listeDesPositions
	 */
	public ArrayList<Position> getListeDesPositions() {
		return listeDesPositions;
	}
	@Override
	public void gererFinLotLocal(String nomLot) throws Exception {
		System.out.println("Les positions trait�es pour lot : "+nomLot+" sont : ");
		for (Position p : listeDesPositions) {
			System.out.println("-"+p);
			
		}
	}
	
}
