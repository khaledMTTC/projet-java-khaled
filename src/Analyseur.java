

import java.util.*;
import java.util.Map.Entry;
import com.google.common.collect.Multimap;

/** Réaliser un traitement sur les données d'une source. */
public class Analyseur {
	private Traitement traitement;

	public Analyseur(Traitement traitement) throws Throwable {
		Objects.requireNonNull(traitement);
		new CycleException().checkCycle(traitement);
		this.traitement = traitement;
	}

	/** Charger l'analyseur avec les données de la source. 
	 * @throws Exception */
	public void traiter(Multimap<Position, Double> source, String nom) throws Exception {

		traitement.donneeTraitee = source;
		for (Entry<Position, Double> iterable_element : traitement.donneeTraitee.entries()) {
			Position p = iterable_element.getKey();
			Double valeur = iterable_element.getValue();
			traitement.traiter(p, valeur);
		}

		/*
		 * this.traitement.donneeTraitee.entrySet().forEach(entry -> { Double valeur =
		 * entry.getValue(); Position p = entry.getKey(); traitement.traiter(p, valeur);
		 * });
		 */

		for (Traitement trait : traitement.suivants) {
			trait.donneeTraitee = traitement.donneeTraitee;
			traiterChaine(trait, trait.donneeTraitee);
		}
		traitement.gererFinLot(nom);
	}

	/** Lancer la chaine de trairtement 
	 * @throws Exception */
	public void traiterChaine(Traitement trait, Multimap<Position, Double> donneeTraitee) throws Exception {

		for (Entry<Position, Double> iterable_element : trait.donneeTraitee.entries()) {
			Position p = iterable_element.getKey();
			Double valeur = iterable_element.getValue();
			trait.traiter(p, valeur);
		}

		/*
		 * trait.donneeTraitee.entrySet().forEach(entry -> { Double valeur =
		 * entry.getValue(); Position p = entry.getKey(); trait.traiter(p, valeur); });
		 */
		for (Traitement trait_suivant : trait.suivants) {
			trait_suivant.donneeTraitee = trait.donneeTraitee;
			traiterChaine(trait_suivant, trait.donneeTraitee);
		}

	}

}
