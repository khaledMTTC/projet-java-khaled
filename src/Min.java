

/**
 * Max calcule le min des valeurs vues, quelque soit le lot.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class Min extends Traitement {

	private double min = Double.POSITIVE_INFINITY;

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		if (min < this.min)
			this.min = min;

	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": mmin = " + this.min);
	}

	public Double trouverMax() {
		for (Double valeur : donneeTraitee.values()) {
			setMin(valeur);
		}
		return getMin();
	}
}
