
import java.lang.reflect.*;
import java.util.*;

/**
 * TraitementBuilder
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class TraitementBuilder {

	private static java.lang.String inputMot;

	/**
	 * Retourne un objet de type Class correspondant au nom en paramètre. Exemples
	 * : - int donne int.class - Normaliseur donne Normaliseur.class
	 */
	Class<?> analyserType(String nomType) throws ClassNotFoundException {
		return Class.forName(nomType);
	}

	/**
	 * Crée l'objet java qui correspond au type formel en exploitant le « mot »
	 * suviant du scanner. Exemple : si formel est int.class, le mot suivant doit
	 * être un entier et le résulat est l'entier correspondant. Ici, on peut se
	 * limiter aux types utlisés dans le projet : int, double et String.
	 */
	static Object decoderEffectif(Class<?> formel, Scanner in) {

		if (formel.equals(Integer.TYPE)) {
			inputMot = in.next();
			return Integer.parseInt(inputMot);
		} else if (formel.equals(Double.TYPE)) {
			inputMot = in.next();
			return Double.parseDouble(inputMot);
		} else if (formel.equals(String.class)) {
			inputMot = in.next();
			return inputMot;
		} else
			return null;
	}

	/**
	 * Définition de la signature, les paramètres formels, mais aussi les
	 * paramètres formels.
	 */
	static private class Signature {
		Class<?>[] formels;
		Object[] effectifs;

		public Signature(Class<?>[] formels, Object[] effectifs) {
			this.formels = formels;
			this.effectifs = effectifs;
		}
	}

	/**
	 * Analyser une signature pour retrouver les paramètres formels et les
	 * paramètres effectifs. Exemple « 3 double 0.0 String xyz int -5 » donne -
	 * [double.class, String.class, int.class] pour les paramètres effectifs et -
	 * [0.0, "xyz", -5] pour les paramètres formels.
	 */
	Signature analyserSignature(Scanner in) throws ClassNotFoundException {
		int inputTaille = in.nextInt();

		Class<?>[] formels = new Class<?>[inputTaille];
		Object[] effectifs = new Object[inputTaille];
		if (formels.length == 0) {
			return new Signature(formels, effectifs);
		} else {
			Map<String, Class> builtInMap = new HashMap<String, Class>();
			{
				builtInMap.put("int", Integer.TYPE);
				builtInMap.put("double", Double.TYPE);
			}

			int countFormel = 0;
			int countE = 0;
			while (inputTaille-->0) {
				inputMot = in.next();
				if (builtInMap.containsKey(inputMot)) {
					formels[countFormel] = builtInMap.get(inputMot);
					effectifs[countE] = decoderEffectif(formels[countE], in);
					countE++;
					countFormel++;
				} else {
					if(inputMot.contentEquals("java.lang.String"))
						formels[countFormel] = analyserType(inputMot);
					else
						formels[countFormel] = analyserType("java.lang." + inputMot);
					effectifs[countE] = decoderEffectif(formels[countFormel], in);
					countE++;
					countFormel++;
				}
			}
			return new Signature(formels, effectifs);
		}

	}

	/**
	 * Analyser la création d'un objet. Exemple : « Normaliseur 2 double 0.0
	 * double 100.0 » consiste à charger la classe Normaliseur, trouver le
	 * constructeur qui prend 2 double, et l'appeler en lui fournissant 0.0 et 100.0
	 * comme paramètres effectifs.
	 */
	Object analyserCreation(Scanner in) throws ClassNotFoundException, InvocationTargetException,
			IllegalAccessException, NoSuchMethodException, InstantiationException {
		inputMot = in.next();
		Class<?> classe = analyserType(inputMot);
		Signature signature = analyserSignature(in);
		Constructor<?> constructor = classe.getConstructor(signature.formels);
		Object t = constructor.newInstance(signature.effectifs);
		return t;
	}

	/**
	 * Analyser un traitement. Exemples : - « Somme 0 0 » - « SupprimerPlusGrand
	 * 1 double 99.99 0 » - « Somme 0 1 Max 0 0 » - « Somme 0 2 Max 0 0
	 * SupprimerPlusGrand 1 double 20.0 0 » - « Somme 0 2 Max 0 0
	 * SupprimerPlusGrand 1 double 20.0 1 Positions 0 0 »
	 * 
	 * @param in  le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	Traitement analyserTraitement(Scanner in, Map<String, Traitement> env) throws ClassNotFoundException,
			InvocationTargetException, IllegalAccessException, NoSuchMethodException, InstantiationException {
		Traitement traitement = (Traitement) analyserCreation(in);
		inputMot = in.next();
		int nombreDeSuivants = Integer.parseInt(inputMot);
		while (nombreDeSuivants-- > 0) {
			traitement.ajouterSuivants(analyserTraitement(in, env));
		}
		return traitement;
	}

	/**
	 * Analyser un traitement.
	 * 
	 * @param in  le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	public Traitement traitement(Scanner in, Map<String, Traitement> env) {
		try {
			return analyserTraitement(in, env);
		} catch (Exception e) {
			throw new RuntimeException("Erreur sur l'analyse du traitement, " + "voir la cause ci-dessous", e);
		}
	}

	public static void main(String[] args) {
		TraitementBuilder builder = new TraitementBuilder();
		try {
			builder.analyserSignature(new Scanner("3 double 0.0 String xyz int -5"));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
