
import java.util.*;
import java.util.Map.Entry;

import javax.lang.model.element.Element;

/**
 * SommeParPosition
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class SommeParPosition extends Traitement {

	private HashMap<Position, Double> listOfPosition = new HashMap<>();
	private boolean done = true;

	public HashMap<Position, Double> getList() {
		return listOfPosition;
	}

	public void calculateSommeParPosition() {
		/*
		 * if (this.listOfPosition.containsKey(p)) { this.listOfPosition.replace(p,
		 * this.listOfPosition.get(p) + valeur); } else { this.listOfPosition.put(p,
		 * valeur); }
		 */
		if (done) {
			for (Entry<Position, Double> element : donneeTraitee.entries()) {
				if (listOfPosition.containsKey(element.getKey())) {
					listOfPosition.replace(element.getKey(), element.getValue() + listOfPosition.get(element.getKey()));
				} else {
					listOfPosition.put(element.getKey(), element.getValue());
				}
			}

			done = false;
		}

	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println("SommeParPartition " + nomLot + ": ");
		this.listOfPosition.entrySet().forEach(entry -> {
			System.out.println("\t-" + entry.getKey() + " -> " + entry.getValue());
		});
		System.out.println("Fin SommeParPartition.");
	}

}
