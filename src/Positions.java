

import java.util.*;

/**
 * Positions enregistre toutes les positions, quelque soit le lot.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class Positions extends PositionsAbstrait {
	
	private ArrayList<Position> positionsList = new ArrayList<>();

	public ArrayList<Position> getPositionsList() {
		return positionsList;
	}

	public void addPositionsList(Position position) {
		this.positionsList.add(position);
	}

	@Override
	public int nombre() {
		return positionsList.size();
	}

	@Override
	public Position position(int indice) {
		return positionsList.get(indice);
	}

	@Override
	public int frequence(Position position) {
		if(position.equals(null))
			return 0;
		int freq = 0;
		for (Position posi : positionsList) {
			if (posi.x == position.x && posi.y == position.y)
				freq++;
		}

		return freq;
	}

}
