

import java.io.File;
import java.util.Scanner;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import data.*;;

/**
 * GenerateurXML écrit dans un fichier, à charque fin de lot, toutes les
 * données lues en indiquant le lot dans le fichier XML.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class GenerateurXML extends Donnees {
	private String nomFichier;
	private boolean done = true;

	public GenerateurXML(String nomFichier) {
		this.nomFichier = nomFichier;
	}

	public void generXmlFichier(String nom) throws Exception {
		if (done) {
			ObjectFactory lot = new ObjectFactory();
			Lot dataLot = lot.createLot();
			dataLot.setName(nom);
			int i = 1;
			for (Entry<Position, Double> element : donneeTraitee.entries()) {
				Donnee donnee = lot.createDonnee();
				Point position = lot.createPoint();
				position.setAbscisse(Integer.toString(element.getKey().getX()));
				position.setOrdonnee(Integer.toString(element.getKey().getY()));
				donnee.setId(Integer.toString(i));
				donnee.setPoint(position);
				donnee.setValeur(Double.toString(element.getValue()));
				dataLot.getDonnee().add(donnee);
				i++;

			}
			JAXBContext context = JAXBContext.newInstance(Lot.class);
			Marshaller marshaller = context.createMarshaller();
			// For formatted output of xml
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			// Create xml file object
			File XMLfile = new File(nomFichier);

			// Java object to XML file
			marshaller.marshal(dataLot, XMLfile);
			// marshaller.marshal(dataLot, System.out);
			done = false;
		}
	}

	@Override
	public void gererFinLotLocal(String nomLot) throws Exception {
		generXmlFichier(nomLot);
		System.out.println("xml fichier � �t� cr�e '" + this.nomFichier + "'");
	}

	/**
	 * @return the nomFichier
	 */
	public String getNomFichier() {
		return nomFichier;
	}

	/**
	 * @param nomFichier the nomFichier to set
	 */
	public void setNomFichier(String nomFichier) {
		this.nomFichier = nomFichier;
	}
	
}
