

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * SommeTest
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class SommeTest extends TraitementTestAbstrait {

	private static final double EPSILON = 1e-8;

	private SommeAbstrait somme;

	@Override
	protected SommeAbstrait nouveauTraitement() {
		return new FabriqueTraitementConcrete().somme();
	}

	@Override
	public void setUp() {
		try {
			super.setUp();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.somme = nouveauTraitement();
	}

	@Test
	public void testerChainage() {
		try {
			testerChainage(true, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testerSommeNominal() throws Exception {
		Position p1 = new Position(1, 2);
		assertEquals(0, this.somme.somme(), 0.0);
		this.somme.gererDebutLot("Lot1");
		this.somme.traiter(p1, 11);
		assertEquals(11, this.somme.somme(), EPSILON);
		this.somme.traiter(p1, 7);
		assertEquals(18, this.somme.somme(), EPSILON);
		this.somme.gererFinLot("Lot1");
	}

}
