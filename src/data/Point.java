//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2020.04.17 � 01:01:30 AM CEST 
//


package data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "abscisse",
    "ordonnee"
})
@XmlRootElement(name = "point")
public class Point {

    @XmlElement(required = true)
    protected String abscisse;
    @XmlElement(required = true)
    protected String ordonnee;

    /**
     * Obtient la valeur de la propri�t� abscisse.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbscisse() {
        return abscisse;
    }

    /**
     * D�finit la valeur de la propri�t� abscisse.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbscisse(String value) {
        this.abscisse = value;
    }

    /**
     * Obtient la valeur de la propri�t� ordonnee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdonnee() {
        return ordonnee;
    }

    /**
     * D�finit la valeur de la propri�t� ordonnee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdonnee(String value) {
        this.ordonnee = value;
    }

}
