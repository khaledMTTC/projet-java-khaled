

import java.util.Map.Entry;

import com.google.common.collect.Multimap;

import com.google.common.collect.TreeMultimap;

/**
 * Normaliseur normalise les données d'un lot en utilisant une transformation
 * affine.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Normaliseur extends Traitement {
	// private HashMap<Double, Double> listOfValues = new HashMap<>();
	private double debut;
	private double fin;
	private boolean done = true;

	public Normaliseur(double debut, double fin) {

		this.debut = debut;
		this.fin = fin;
		/*
		 * for (AbstractMap.SimpleImmutableEntry<Position,Double> iterable_element : ) {
		 * max=Math.max(max,iterable_element.getValue());
		 * min=-Math.max(min,iterable_element.getValue()); }
		 */
	}

	public void NormaliserValeurs() {
		Multimap<Position, Double> donneeTraiteeTemp = TreeMultimap.create();
		if (done) {
			for (Entry<Position, Double> element : donneeTraitee.entries()) {
				double valeurNonNormalisee = element.getValue();
				double valeurNrmalisee = 0;
				double a = (renvoiMax(donneeTraitee) - renvoiMin(donneeTraitee)) / (fin - debut);
				double b = debut - (a * renvoiMin(donneeTraitee));
				valeurNrmalisee = a * valeurNonNormalisee + b;
				// listOfValues.put(valeurNonNormalisee, valeurNrmalisee);
				donneeTraiteeTemp.put(element.getKey(), valeurNrmalisee);
			}
			donneeTraitee = donneeTraiteeTemp;
			done = false;
		}
	}

	private Double renvoiMax(Multimap<Position, Double> donnee) {
		Max max = new Max();
		max.donneeTraitee = donnee;
		return max.trouverMax();
	}

	private Double renvoiMin(Multimap<Position, Double> donnee) {
		Min min = new Min();
		min.donneeTraitee = donnee;
		return min.trouverMax();
	}

	public double getDebut() {
		return debut;
	}

	public void setDebut(double debut) {
		this.debut = debut;
	}

	public double getFin() {
		return fin;
	}

	public void setFin(double fin) {
		this.fin = fin;
	}

}
