

import java.util.Scanner;

/**
 * ExempleAnalyse
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class ExempleAnalyse {

	public static void exemple1() throws Throwable {
		System.out.println();
		System.out.println("=== exemple1() ===");
		Scanner fileObj = new Scanner(System.in); // Create a Scanner object for File
		System.out.println("Please enter file's name from which you will import data into your treat :");
		String fileNom = fileObj.nextLine();
		String nomFichierXml1;
		String nomFichierXml2;

		FabriqueTraitement traitements = new FabriqueTraitementConcrete();
		// Construire le traitement

		Donnees donnees = traitements.donnees();

		SommeAbstrait somme = traitements.somme();
		SommeAbstrait sommeApresSupprimerPlusGrand = traitements.somme();
		SommeAbstrait sommeApresNormaliseur = traitements.somme();

		PositionsAbstrait positions = traitements.positions();
		PositionsAbstrait positionsApresSupprimerPlusGrand = traitements.positions();
		PositionsAbstrait positionsApresNormaliseur = traitements.positions();

		SupprimerPlusGrand supprimerPlusGrand = traitements.supprimerPlusGrand(10);
		SupprimerPlusPetit supprimerPlusPetit = traitements.supprimerPlusPetit(0);

		Multiplicateur multiplicateurApresNormaliseur = traitements.multiplicateur(10);

		SommeParPosition sommeParPosition = traitements.sommeParPosition();
		SommeParPosition sommeParPositionApresSupprimerPlusGrand = traitements.sommeParPosition();
		SommeParPosition sommeParPositionApresNormaliseur = traitements.sommeParPosition();

		Normaliseur normaliseur = new Normaliseur(0, 100);
		

		Max max = traitements.max();
		Max maxApresSupprimerPlusGrand = traitements.max();
		Max maxApresNormaliseur = traitements.max();
		System.out.println("Please enter the name of your xml file (example : 'example.xml' : ");
		nomFichierXml1 = fileObj.nextLine();
		GenerateurXML generateurXMLApresSupprimerPlusGrand = new GenerateurXML(nomFichierXml1);

		System.out.println("Please enter the name of your 2nd xml file (example : 'example.xml' : ");
		nomFichierXml2 = fileObj.nextLine();
		GenerateurXML generateurXMLApresNormaliser = new GenerateurXML(nomFichierXml2);
		
		
		Maj maj = traitements.maj();

		donnees.ajouterSuivants(
				positions.ajouterSuivants(max.ajouterSuivants(somme.ajouterSuivants(sommeParPosition))));
		donnees.ajouterSuivants(supprimerPlusPetit.ajouterSuivants(supprimerPlusGrand));
		supprimerPlusGrand.ajouterSuivants(generateurXMLApresSupprimerPlusGrand);
		supprimerPlusGrand.ajouterSuivants(
				positionsApresSupprimerPlusGrand.ajouterSuivants(maxApresSupprimerPlusGrand.ajouterSuivants(
						sommeApresSupprimerPlusGrand.ajouterSuivants(sommeParPositionApresSupprimerPlusGrand))));

		donnees.ajouterSuivants(normaliseur);
		normaliseur.ajouterSuivants(generateurXMLApresNormaliser);
		normaliseur.ajouterSuivants(positionsApresNormaliseur
				.ajouterSuivants(multiplicateurApresNormaliseur.ajouterSuivants(maxApresNormaliseur
						.ajouterSuivants(sommeApresNormaliseur.ajouterSuivants(sommeParPositionApresNormaliseur)))));
		normaliseur.ajouterSuivants(maj);
		
		/*
		 * donnees.ajouterSuivants(max); max.ajouterSuivants(somme);
		 * somme.ajouterSuivants(sommeParPosition);
		 * 
		 * donnees.ajouterSuivants(supprimerPlusPetit);
		 * supprimerPlusPetit.ajouterSuivants(supprimerPlusGrand);
		 * supprimerPlusGrand.ajouterSuivants(positions);
		 * positions.ajouterSuivants(max); max.ajouterSuivants(somme);
		 * somme.ajouterSuivants(sommeParPosition);
		 * 
		 * 
		 * supprimerPlusGrand.ajouterSuivants(multiplicateur);
		 * multiplicateur.ajouterSuivants(sommeMultiplicateur);
		 * supprimerPlusGrand.ajouterSuivants(sommeParPosition);
		 * sommeParPosition.ajouterSuivants(max);
		 */

		Analyseur analyseur = new Analyseur(donnees);

		System.out.println("Traitement : " + donnees);

		analyseur.traiter(donnees.getDonneesType1(fileNom), "lot1");

		/*
		 * // Traiter des données manuelles somme.gererDebutLot("manuelles");
		 * somme.traiter(new Position(1, 1), 5.0); somme.traiter(new Position(1, 2),
		 * 2.0); somme.traiter(new Position(1, 1), -1.0); somme.traiter(new Position(1,
		 * 2), 1.5); somme.gererFinLot("manuelles");
		 */
		// Exploiter les résultats
		System.out.println("Somme = " + somme.somme());// ne fonctionne pas parce que
		// j'ai remis la somme � zero � li fin du lot
		System.out.println("Somme apr�s normalisation + = " + sommeApresNormaliseur.somme());// c'est pariel
		System.out.println("Positions.frequence(new Position(1,2)) = " + positions.frequence(new Position(1, 2)));
		fileObj.close();
		
		
		
		
		
		
		
	}

	public static void exemple2(String traitements) throws Throwable {
	
		System.out.println();
		System.out.println("=== exemple2(" + traitements + ") ===");

		// Construire les traitements
		TraitementBuilder builder = new TraitementBuilder();
		Traitement main = builder.traitement(new java.util.Scanner(traitements), null);

		System.out.println("Traitement : " + main);

		// Construire l'analyseur
		Analyseur analyseur = new Analyseur(main);
		analyseur.traiter(new Donnees().getDonneesType1("donnees.txt"), "lot2");

		// Traiter les autres sources de données : "donnees.txt", etc.
	}

	public static void main(String[] args) throws Throwable {
		//exemple1();
		exemple2("Somme 0 1 Positions 0 0");
		String calculs = "Positions 0 1 Max 0 1 Somme 0 1 SommeParPosition 0";
		String generateur = "GenerateurXML 1 java.lang.String NOM--genere.xml";
		String traitement1 = generateur.replaceAll("NOM", "brut") + " 3" + " " + calculs + " 0" + " "
				+ "SupprimerPlusPetit 1 double 0.0 1 SupprimerPlusGrand 1 double 10.0 2" + " "
				+ generateur.replaceAll("NOM", "valides") + " 0" + " " + calculs + " 0" + " "
				+ "Normaliseur 2 double 0.0 double 100.0 2" + " " + generateur.replaceAll("NOM", "normalisees") + " 0"
				+ " " + calculs + " 0";
		System.out.println(traitement1);
		exemple2(calculs + " 0");
		exemple2(traitement1);
		
		//check Cycle
		FabriqueTraitement trait = new FabriqueTraitementConcrete();
		PositionsAbstrait positions = trait.positions();
		SommeAbstrait somme = trait.somme();
		SommeParPosition sommeParPosition = trait.sommeParPosition();
		somme.ajouterSuivants(positions.ajouterSuivants(sommeParPosition.ajouterSuivants(somme)));
		Analyseur analyseur = new Analyseur(somme);
		
	}

}
