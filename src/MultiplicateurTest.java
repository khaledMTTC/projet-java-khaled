

import org.junit.*;
import static org.junit.Assert.*;

/**
 * MultiplicateurTest
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class MultiplicateurTest extends TraitementTestAbstrait {

	private static final double EPSILON = 1e-8;

	private Multiplicateur multiplicateur;
	private TraitementTestAbstrait.Dernier dernier;

	@Override
	protected Multiplicateur nouveauTraitement() {
		return new FabriqueTraitementConcrete().multiplicateur(10.0);
	}

	@Override
	public void setUp() {
		try {
			super.setUp();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.multiplicateur = nouveauTraitement();
		this.dernier = new TraitementTestAbstrait.Dernier();
		this.multiplicateur.ajouterSuivants(this.dernier);
	}

	@Test
	public void testerMultiplicateurNominal() {
		Position p1 = new Position(1, 2);
		this.multiplicateur.gererDebutLot("Lot1");
		try {
			this.multiplicateur.traiter(p1, 11);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(110, this.dernier.valeur, EPSILON);
		assertSame(p1, this.dernier.position);

		try {
			this.multiplicateur.traiter(p1, 7);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(70, this.dernier.valeur, EPSILON);
		assertSame(p1, this.dernier.position);

		try {
			this.multiplicateur.gererFinLot("Lot1");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
