

import java.util.Map.Entry;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;

/**
 * Multiplicateur transmet la valeur multipliée par un facteur.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Multiplicateur extends Traitement {
	private double facteur = 1;
	private boolean done = true;

	public double getFacteur() {
		return facteur;
	}

	public void setFacteur(double facteur) {
		this.facteur = facteur;
	}

	public Multiplicateur(double facteur) {
		this.facteur = facteur;
	}

	public void multiplierValeur() {
		if (done) {
			Multimap<Position, Double> donneeTraiteeTemp = TreeMultimap.create();
			for (Entry<Position, Double> element : donneeTraitee.entries()) {
				donneeTraiteeTemp.put(element.getKey(), element.getValue() * facteur);
			}
			donneeTraitee = donneeTraiteeTemp;
			done = false;
		}
	}
}
