

import java.util.Map.Entry;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;

/**
 * SupprimerPlusGrand supprime les valeurs plus grandes qu'un seuil.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class SupprimerPlusGrand extends Traitement {

	private double seuil;
	private boolean done = true;

	public SupprimerPlusGrand(double seuil) {
		super();
		this.seuil = seuil;
	}

	public double getSeuil() {
		return seuil;
	}

	public void setSeuil(double seuil) {
		this.seuil = seuil;
	}

	public void filtrerLesDonnees() {
		if (done) {
			Multimap<Position, Double> donneeTraiteeTemp = TreeMultimap.create();
			for (Entry<Position, Double> element : donneeTraitee.entries()) {
				if (element.getValue() <= seuil) {
					donneeTraiteeTemp.put(element.getKey(), element.getValue());
				}
			}
			donneeTraitee = donneeTraiteeTemp;
			done = false;
		}
	}

}
